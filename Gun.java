import javax.sound.sampled.*;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.LineEvent.Type;

public class Gun {
	
	public String image, name;
	public int left, max, rounds;
	private int firetime, reloadtime;
	private long lastFireTime;
	private long lastReloadTime;
	
	public Gun(String image, String name, int m, int r, int ft, int rt) {
		this.name = name;
		this.image = image;
		left = m;
		max = m;
		rounds = r;
		firetime = ft;
		reloadtime = rt;
		lastFireTime = 0;
		lastReloadTime = -1000;
		
	}
	
	public String image() {
		return image;
	}
	
	public boolean canShoot() {
		long currentTime = System.currentTimeMillis();
		return (lastFireTime + firetime < currentTime
				&& lastReloadTime + reloadtime < currentTime
				&& left > 0);
	}
	
	public boolean canReload() {
		long currentTime = System.currentTimeMillis();
		return (lastFireTime + firetime < currentTime
				&& lastReloadTime + reloadtime < currentTime
				&& rounds > 0);
	}
	
	public void fire() {
		left--;
		lastFireTime = System.currentTimeMillis();
		play("gun-gunshot-01.wav");

	}
	
	public void reload() {
		lastReloadTime = System.currentTimeMillis();
		rounds--;
		left = max;
	}
	
	public String toString() {
		return "GUN: "+name+"\nBULLETS: " + left+"/"+max+"\nROUNDS: " + rounds;
	}
	
	public String status() {
		long currentTime = System.currentTimeMillis();
		if (lastFireTime + firetime > currentTime) return "Firing...";
		if (lastReloadTime + reloadtime > currentTime) return "Reloading....";
		return "Gun Ready";
	}
	


public static void play(String filename)
{
    try
    {
        Clip clip = AudioSystem.getClip();
        clip.open(AudioSystem.getAudioInputStream(new File(filename)));
        clip.start();
    }
    catch (Exception exc)
    {
        exc.printStackTrace(System.out);
    }
}

}