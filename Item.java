import java.awt.Color;

public interface Item {
	public void use(Player p);
	public void onPickup(Player p);
	public boolean status();
	public double getX();
	public double getY();
	public String image();
	public void not();
	public double distance(Player p);

	
	
}