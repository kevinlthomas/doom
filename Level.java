import java.util.ArrayList;

public class Level {
	
	public String name;
	public int[][] worldMap;
	public ArrayList<Item> items;
	public ArrayList<Monster> monsters;
	public double startX, startY, dirx, diry;
	
	
	public Level (String n, int[][] wmp, double sx, double sy,
			double dx, double dy, ArrayList<Item> i, ArrayList<Monster> m) {
		name = n;
		worldMap = wmp;
		startX = sx;
		startY = sy;
		dirx = dx;
		diry = dy;
		items = i;
		monsters = m;
	}
}