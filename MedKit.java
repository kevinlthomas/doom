import java.awt.Color;

	public class MedKit implements Item, Sprite {
		
		public String name;
		public boolean available;
		public String pic;
		public static double x, y;
		
		public MedKit(double x2, double y2) {
		name = "Medical Kit";
		pic = "medkit.png";
		available = true;
		x = x2;
		y = y2;
		}
		
		public String toString() {
			return "MedKit<" + x + "," + y + ">";
		}
		
		public double getX() {
			return x;
		}
		
		
		public double getY() {
			return y;
		}
		
		public void not() {
			available = false;
		}
		
		public boolean status() {
			return available;
		}


		public void use(Player p) {
			// TODO Auto-generated method stub
			
		}

		public String image() {
			return pic;
		}
		
		public double distance(Player p) {
			double dx = this.x - p.getX();
			double dy = this.y - p.getY();
			return Math.sqrt(dx*dx+dy*dy);
		}

		public void onPickup(Player p) {
			// TODO Auto-generated method stub
			
		}

	}