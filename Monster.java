public class Monster implements Sprite {
	
	private boolean status;
	private double speed, range;
	private String image;
	private boolean attacking;
	
	private double x, y;
	
	public Monster(double x, double y, double speed, double range, String image) {
		this.speed = speed;
		this.image = image;
		this.range = range;
		status = true;
		this.x = x;
		this.y = y;
		attacking = false;
	}
	
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public boolean status() {
		return status;
	}
	
	public void advance(Player p, int[][] worldMap) {
		if (attacking) {
			double dx = p.getX() - this.x;
			double dy = p.getY() - this.y;
			double mag = Math.sqrt(dx*dx+dy*dy);
			double dxunit = dx / mag;
			double dyunit = dy / mag;
			 if(worldMap.length > (int)(x + dxunit*speed) && worldMap[0].length > (int)y)
			 if(worldMap[(int)(x + dxunit*speed)][(int)(y)]%100 == 0)
			x += dxunit*speed;
			 if(worldMap[0].length > (int)(y + dyunit*speed) && worldMap.length > (int)x)
			 if(worldMap[(int)(x)][(int)(y + dyunit*speed)]%100 == 0)
			y += dyunit*speed;
		}
	}
	
	public double range() {
		return range;
	}
	
	public void alert() {
		attacking = true;
	}
	
	public void kill() {
		status = false;
	}
	
	public double distance(Player p) {
		double dx = this.x - p.getX();
		double dy = this.y - p.getY();
		return Math.sqrt(dx*dx+dy*dy);
	}
	
	public String toString() {
		return "Monster<"+x+","+y+">";
	}
	
	public String image() {
		return image;
	}
	
}