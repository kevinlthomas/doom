import java.util.ArrayList;

public class Player {
	
	int score;
	Gun gun;
	double x, y;
	String name;
	int health;
	ArrayList<Item> backpack;
	int keys;
	
	public Player(String n) {
		score = 0;
		health = 100;
		name = n;
		keys = 5; // RESETTTTTT **********!!!!!!!!!!!!!!!!!***************
		backpack = new ArrayList<Item>();
		gun = null;
		
		
	}
	
	public void setGun(Gun g) {
		gun = g;
	}
	
	public void setPos(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
}