# DOOM #

This is a Doom style maze game created using raycasting.

## Controls

Up Arrow - Forward  
Back Arrow - Back  
Left Arrow - Look Left  
Right Arrow - Look Right  
A - Slide Left  
D - Slide Right  
Space Bar - Shoot  
R - Reload  
U - Unlock  
Q - Quit  

## Goal
Navigate each level to find the exit represented by a giant green cube. Walk into the cube to exit. Unlock blue walls by facing the wall and pressing the unlock key. Shoot the monsters before they get you. You only have one life.
