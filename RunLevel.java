import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.TreeSet;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;

public class RunLevel  extends JComponent implements KeyListener {
    private static Object keyLock = new Object();
    private static LinkedList<Character> keysTyped = new LinkedList<Character>();
    private static TreeSet<Integer> keysDown = new TreeSet<Integer>();
    
    static int mapWidth;
    static int mapHeight;
    static Monster monsterInSight;
	static int w = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	static int h = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
    static int nextLockedDoor = 99;
    static Level l;
    static Player p;
    static ArrayList<Item> items;
	static ArrayList<Monster> monsters;
    static double[] wallDist = new double[w];
    static double[] wallStart = new double[w];
    static double[] wallEnd = new double[w];
    static Color[] wallColor = new Color[w];
    static Monster deadMonster = new Monster(0,0,0,0,"");
    
    static boolean isGameOver = false;
    
    public static double speedd, speeda, posX, posY, dirX, dirY, planeX, planeY, time, oldTime;
	
    //settings 
	static double moveSpeed = .2;
	static double rotSpeed = .1;
	// controls : forward back turnleft turnright fire moveleft moveright unlock fire reload pause
    static int[] k = {38, 40, 37, 39, 32, 65, 68, 85, 32, 82, 93};
    
	public void paintComponent(Graphics gc) {
		//blank background
		gc.setColor(Color.black);
		gc.fillRect(0, 0, w, h);
		gc.setColor(Color.white);
		
		//fill the arrays for the item drawing
		
		

		double[][] itemInfo = new double[w][2];
		for (int x = 0; x < w; x++) {
			// find the distance to the object in that column;
			
			
			
			                                 
		      //calculate ray position and direction
		      double cameraX = 2 * x / (double)w - 1; //x-coordinate in camera space
		      double rayPosX = posX;
		      double rayPosY = posY;
		      double rayDirX = dirX + planeX * cameraX;
		      double rayDirY = dirY + planeY * cameraX;
		      
		      //which box of the map we're in
		      int mapX = (int)rayPosX;
		      int mapY = (int)rayPosY;

		      //length of ray from current position to next x or y-side
		      double sideDistX;
		      double sideDistY;

		       //length of ray from one x or y-side to next x or y-side
		      double deltaDistX = Math.sqrt(1 + (rayDirY * rayDirY) / (rayDirX * rayDirX));
		      double deltaDistY = Math.sqrt(1 + (rayDirX * rayDirX) / (rayDirY * rayDirY));
		      double perpWallDist;

		      //what direction to step in x or y-direction (either +1 or -1)
		      int stepX;
		      int stepY;

		      int hit = 0; //was there a wall hit?
		      int side = 2; //was a NS or a EW wall hit?
		      

		      //calculate step and initial sideDist
		      if (rayDirX < 0) {
		        stepX = -1;
		        sideDistX = (rayPosX - mapX) * deltaDistX;
		      } else {
		        stepX = 1;
		        sideDistX = (mapX + 1.0 - rayPosX) * deltaDistX;
		      }
		      if (rayDirY < 0) {
		        stepY = -1;
		        sideDistY = (rayPosY - mapY) * deltaDistY;
		      } else {
		        stepY = 1;
		        sideDistY = (mapY + 1.0 - rayPosY) * deltaDistY;
		      }
		      //perform DDA
		      while (hit == 0)
		      {
		        //jump to next map square, OR in x-direction, OR in y-direction
		        if (sideDistX < sideDistY) {
		          sideDistX += deltaDistX;
		          mapX += stepX;
		          side = 0;
		        } else {
		          sideDistY += deltaDistY;
		          mapY += stepY;
		          side = 1;
		        }
		        //Check if ray has hit a wall
		        if (l.worldMap[mapX][mapY] > 0) hit = 1;
		        // not a wall but an item, store the info
		        if (l.worldMap[mapX][mapY] < 0){
		        	itemInfo[x][0] = l.worldMap[mapX][mapY];
				      if (side == 0) itemInfo[x][1] = 
				    		  (mapX - rayPosX + (1 - stepX) / 2) / rayDirX;
				      else           itemInfo[x][1] =
				    		  (mapY - rayPosY + (1 - stepY) / 2) / rayDirY;
		        	
		        }
		      } 
		      

		      //Calculate distance projected on camera direction
		      if (side == 0) perpWallDist = (mapX - rayPosX + (1 - stepX) / 2) / rayDirX;
		      else           perpWallDist = (mapY - rayPosY + (1 - stepY) / 2) / rayDirY;
		      
		      //Calculate height of line to draw on screen
		      int lineHeight = (int)(h / perpWallDist);

		      //calculate lowest and highest pixel to fill in current stripe
		      int drawStart = -lineHeight / 2 + h / 2;
		      if(drawStart < 0)drawStart = 0;
		      int drawEnd = lineHeight / 2 + h / 2;
		      if(drawEnd >= h)drawEnd = h - 1;
		      
		      // ADD THE CASE SWITCH
		      if (1 == side) gc.setColor(Color.gray);
		      if (0 == side) gc.setColor(Color.lightGray);
		      if (l.worldMap[mapX][mapY] > 50) gc.setColor(Color.BLUE);
		      if (l.worldMap[mapX][mapY] == 100) gc.setColor(Color.GREEN);
		   gc.drawLine(x, drawStart, x, drawEnd);
		      
		      // add it to array of distances
		      wallDist[x] = perpWallDist;
		      wallStart[x] = drawStart;
		      wallEnd[x] = drawEnd;
		      wallColor[x] = gc.getColor();
		      
		}
		// ------------------MONSTER CASTING-----------------------------------
		monsterInSight = deadMonster;
		for (Monster m : combineInOrder(monsters,p)) {
			if (m.status()) {
			double  spriteX = m.getX() - posX;
			double spriteY = m.getY() - posY;
			
		      double invDet = 1.0 / (planeX * dirY - dirX * planeY); //required for correct matrix multiplication

		      double transformX = invDet * (dirY * spriteX - dirX * spriteY);
		      double transformY = invDet * (-planeY * spriteX + planeX * spriteY); //this is actually the depth inside the screen, that what Z is in 3D

		      int spriteScreenX = (int)((w / 2) * (1 + transformX / transformY));
		      gc.setColor(Color.RED);
		      
		      int spriteHeight = Math.abs((int)(h / (transformY))); //using "transformY" instead of the real distance prevents fisheye
		      //calculate lowest and highest pixel to fill in current stripe
		      int drawStartY = -spriteHeight / 2 + h / 2;
		      if(drawStartY < 0) drawStartY = 0;
		      int drawEndY = spriteHeight / 2 + h / 2;
		      if(drawEndY >= h) drawEndY = h - 1;
		      int diff = (drawEndY-drawStartY);
		      double d = Math.sqrt(spriteX*spriteX + spriteY*spriteY);
		      
		     if (transformY > 1)
		    	 if (spriteScreenX >= 0 && spriteScreenX < wallDist.length)
		    		 if (d < wallDist[spriteScreenX]) {
		    			 if (spriteScreenX > (w*.46) && spriteScreenX < (w*.54)) // if monster in gunview
		    				 monsterInSight = m;
		            drawTheImage(gc, m.image(), spriteScreenX, drawStartY, diff, diff);
		            // remember right edge may be past end of screen
		            for (int n = spriteScreenX; (n <= spriteScreenX+diff && n < w); n++) {
				    	 if (spriteScreenX >= 0 && spriteScreenX < wallDist.length)
				    		 if (d > wallDist[n]) {
				    			 gc.setColor(wallColor[n]);
				    			   gc.drawLine(n, (int) wallStart[n], n, (int)wallEnd[n]);
				    		 }
				    			 
		            }
		     }
		     if (transformY > 1) // for when pic starts left of screen
		    	 if (spriteScreenX + diff >= 0 && spriteScreenX < wallDist.length)
		    		 if (d < wallDist[0]) {
		            drawTheImage(gc, m.image(), spriteScreenX, drawStartY, diff, diff);
		            for (int n = 0; (n <= spriteScreenX+diff && n < w); n++) {
				    	 if (spriteScreenX >= 0 && spriteScreenX < wallDist.length)
				    		 if (d > wallDist[n]) {
				    			 gc.setColor(wallColor[n]);
				    			   gc.drawLine(n, (int) wallStart[n], n, (int)wallEnd[n]);
				    		 }
				    			 
		            }
		     }
		     if (Math.abs(m.distance(p)) < 1) {	 
		    	 isGameOver = true;
		     }	
		     if (m.range() > m.distance(p)) m.alert();
		     m.advance(p, l.worldMap);
			}
		}
		
		// ----------------------ITEM CASTING-------------------------------
		
		for (Item it : items) {
			if (it.status()) {
			double  spriteX = it.getX() - posX;
			double spriteY = it.getY() - posY;
			
		      double invDet = 1.0 / (planeX * dirY - dirX * planeY); //required for correct matrix multiplication

		      double transformX = invDet * (dirY * spriteX - dirX * spriteY);
		      double transformY = invDet * (-planeY * spriteX + planeX * spriteY); //this is actually the depth inside the screen, that what Z is in 3D

		      int spriteScreenX = (int)((w / 2) * (1 + transformX / transformY));
		      gc.setColor(Color.RED);
		      
		      int spriteHeight = Math.abs((int)(h / (transformY))); //using "transformY" instead of the real distance prevents fisheye
		      //calculate lowest and highest pixel to fill in current stripe
		      int drawStartY = -spriteHeight / 2 + h / 2;
		      if(drawStartY < 0) drawStartY = 0;
		      int drawEndY = spriteHeight / 2 + h / 2;
		      if(drawEndY >= h) drawEndY = h - 1;
		      int diff = (drawEndY-drawStartY);
		      double d = Math.sqrt(spriteX*spriteX + spriteY*spriteY);
		      double dl = Math.sqrt(spriteX*spriteX - diff +diff*diff + spriteY*spriteY);
		      double dr = Math.sqrt(spriteX*spriteX + diff +diff*diff + spriteY*spriteY);
		      
		     if (transformY > 1)
		    	 if (spriteScreenX >= 0 && spriteScreenX < wallDist.length)
		    		// if (d < wallDist[spriteScreenX]) {
		            drawTheImage(gc, it.image(), spriteScreenX, drawStartY, diff, diff);
		            // remember right edge may be past end of screen
		            for (int n = spriteScreenX; (n <= spriteScreenX+diff && n < w); n++) {
				    	 if (spriteScreenX >= 0 && spriteScreenX < wallDist.length)
				    		 if (d > wallDist[n]) {
				    			 gc.setColor(wallColor[n]);
				    			   gc.drawLine(n, (int) wallStart[n], n, (int)wallEnd[n]);
				    		 }
				    			 
		            }
		     //}
		     if (transformY > 1) // for when pic starts left of screen
		    	 if (spriteScreenX + diff >= 0 && spriteScreenX < wallDist.length)
		    	//	 if (d < wallDist[0]) {
		            drawTheImage(gc, it.image(), spriteScreenX, drawStartY, diff, diff);
		            for (int n = 0; (n <= spriteScreenX+diff && n < w); n++) {
				    	 if (spriteScreenX >= 0 && spriteScreenX < wallDist.length)
				    		 if (d > wallDist[n]) {
				    			 gc.setColor(wallColor[n]);
				    			   gc.drawLine(n, (int) wallStart[n], n, (int)wallEnd[n]);
				    		 }
				    			 
		            }
		    // }
			}
		}
		
		// static graphics
		gc.setColor(Color.white);
		gc.fillRect(0,h-300,300,300);
		gc.setFont(new Font("TimesRoman", Font.PLAIN, 30)); 
		gc.setColor(Color.black);
		gc.drawString("   GUN: " + p.gun.name, 50, h - 260);
		gc.drawString("  AMMO: " + p.gun.left + "/" + p.gun.max, 50, h - 220);
		gc.drawString("ROUNDS: " + p.gun.rounds, 50, h - 180);
		gc.drawString(p.gun.status(), 50, h - 140);
		gc.drawLine(0, h-130, 300, h-130);
        drawTheImage(gc, p.gun.image(), w/2-100, h-450, 450, 450);
        // compass
        gc.setColor(Color.white);
        gc.drawOval(w-100, 0, 100, 100);
        gc.drawLine(w-50, 50, (int)(w-50+dirY*50),(int)(50+dirX*50));
	}
	
	public static boolean startLevel(Level lev, Player pl) {
		RunLevel u = new RunLevel();
		System.out.println(u.w);
		System.out.println(u.h);
		JFrame f = new JFrame();
		f.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
		f.add(u);
		f.setUndecorated(true);
		f.setVisible(true);
		f.addKeyListener(u);
		
		// load level
		l = lev;
		monsters = l.monsters;
		p = pl;
		
		posX = l.startX;
		posY = l.startY;
		p.setPos(posX, posY);
		dirX = -1;
		dirY = 0;
		items = l.items;
		mapWidth = l.worldMap.length;
		mapHeight = l.worldMap[0].length;
		planeX = 0;
		planeY = .66;
		time = 0;
		oldTime = 0;
		nextLockedDoor = 99;
		// ----------------------------------MOUSE HIDING----------------------
		// Transparent 16 x 16 pixel cursor image.
		BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

		// Create a new blank cursor.
		Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
		    cursorImg, new Point(0, 0), "blank cursor");

		// Set the blank cursor to the JFrame.
		f.getContentPane().setCursor(blankCursor);
		// --------------------------------------------------------------------
		while (true) {
			
			if(isKeyPressed(81)) {
                f.setVisible(false);
				return false;
			}
			if(isKeyPressed(k[0])) {
				 if(l.worldMap[(int)(posX + dirX * moveSpeed)][(int)(posY)]%100 == 0)
			      posX += dirX * moveSpeed;
				 if(l.worldMap[(int)(posX)][(int)(posY + dirY * moveSpeed)]%100 == 0)
					 posY += dirY * moveSpeed;
			}
			if(isKeyPressed(k[2])) {
			    
			        //both camera direction and camera plane must be rotated
			        double oldDirX = dirX;
			        dirX = dirX * Math.cos(rotSpeed) - dirY * Math.sin(rotSpeed);
			        dirY = oldDirX * Math.sin(rotSpeed) + dirY * Math.cos(rotSpeed);
			        double oldPlaneX = planeX;
			        planeX = planeX * Math.cos(rotSpeed) - planeY * Math.sin(rotSpeed);
			        planeY = oldPlaneX * Math.sin(rotSpeed) + planeY * Math.cos(rotSpeed);
			}
			if(isKeyPressed(k[3])) {
			      double oldDirX = dirX;
			      dirX = dirX * Math.cos(-rotSpeed) - dirY * Math.sin(-rotSpeed);
			      dirY = oldDirX * Math.sin(-rotSpeed) + dirY * Math.cos(-rotSpeed);
			      double oldPlaneX = planeX;
			      planeX = planeX * Math.cos(-rotSpeed) - planeY * Math.sin(-rotSpeed);
			      planeY = oldPlaneX * Math.sin(-rotSpeed) + planeY * Math.cos(-rotSpeed);
			}
			if(isKeyPressed(k[1])) {
				 if(l.worldMap[(int)(posX - dirX * moveSpeed)][(int)(posY)]%100 == 0)
				      posX -= dirX * moveSpeed;
					 if(l.worldMap[(int)(posX)][(int)(posY - dirY * moveSpeed)]%100 == 0)
						 posY -= dirY * moveSpeed;
			}
			if(isKeyPressed(k[5])) {
				 if(l.worldMap[(int)(posX - dirX * moveSpeed)][(int)(posY)]%100 == 0)
				      posX -= dirY * moveSpeed / 3;
					 if(l.worldMap[(int)(posX)][(int)(posY - dirY * moveSpeed)]%100 == 0)
						 posY += dirX * moveSpeed / 3;
			}
			if(isKeyPressed(k[6])) {
				 if(l.worldMap[(int)(posX - dirX * moveSpeed)][(int)(posY)]%100 == 0)
				      posX += dirY * moveSpeed / 3;
					 if(l.worldMap[(int)(posX)][(int)(posY - dirY * moveSpeed)]%100 == 0)
						 posY -= dirX * moveSpeed / 3;
			}
			if(isKeyPressed(k[7])) {
				 if (p.keys > 0) {
					 for  (int v = 0; v < mapWidth; v++) {
						 for (int w = 0; w < mapHeight; w++) {
							 if (l.worldMap[v][w] == nextLockedDoor) {
								 l.worldMap[v][w] = 0;
							 }
						 }
					 }
					 p.keys--;
					 nextLockedDoor--;
				 }
			}
			if (isKeyPressed(k[8])) {
				if (p.gun.canShoot()) {
					monsterInSight.kill();
					monsterInSight = deadMonster;
					p.gun.fire();
				}    
				
			}
			if (isKeyPressed(k[9])) {
				if (p.gun.canReload()) {
					p.gun.reload();
				}    
			}
			if (isKeyPressed(k[10])) {
				// PAUSE
			}
			
			u.paintImmediately(0,0,w,h);
			if (isGameOver)  {
                f.setVisible(false);
				return true;
			}
			p.setPos(posX, posY);
			// check if winner
			if (l.worldMap[(int)posX][(int)posY] == 100) {
                f.setVisible(false);
				return true;
			}
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	
    public static boolean isKeyPressed(int keycode) {
        synchronized (keyLock) {
            return keysDown.contains(keycode);
        }
    }


    /**
     * This method cannot be called directly.
     */
    public void keyTyped(KeyEvent e) {
        synchronized (keyLock) {
            keysTyped.addFirst(e.getKeyChar());
        }
    }

    /**
     * This method cannot be called directly.
     */
    public void keyPressed(KeyEvent e) {
        synchronized (keyLock) {
            keysDown.add(e.getKeyCode());
        }
    }

    /**
     * This method cannot be called directly.
     */
    public void keyReleased(KeyEvent e) {
        synchronized (keyLock) {
            keysDown.remove(e.getKeyCode());
        }
    }
    
    public static void pause(int p) {
    	try {
			Thread.sleep(p);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void drawTheImage(Graphics g, String file, int x, int y, int w, int h) {
        try {
			BufferedImage img = ImageIO.read(new File(file));
			BufferedImage bi = new
					BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
			g.drawImage(img, x-w/2, y, w, h, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public ArrayList<Monster> combineInOrder(ArrayList<Monster> m, Player p) {
    	ArrayList<Monster> sprites = new ArrayList<Monster>();

    	for (Monster it : m) {
               sprites = insertInOrder(it, sprites, p);
    	}
    	return sprites;
    }
    
    public ArrayList<Monster> insertInOrder(Monster it, ArrayList<Monster> sprites, Player p) {
    	if (sprites.size() == 0) {
    		sprites.add(it);
    		return sprites;
    	}
    	for (int n = 0; n < sprites.size(); n++) {
    		if (sprites.get(n).distance(p) < it.distance(p)) {
    			sprites.add(n,it);
    			return sprites;
    		}
    	}
    	sprites.add(sprites.size(), it);
    	return sprites;
    }


}