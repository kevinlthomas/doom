public interface Sprite {
	public String image();
	public double getX();
	public double getY();
	public double distance(Player p);
	public boolean status();
}