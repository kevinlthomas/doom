import java.awt.Color;
import java.awt.Graphics;

public class VLine  {
	static int start, end, col;
	static Color color;
	
	public VLine(Color cl, int c, int s, int e) {
		start = s;
		col = c;
		end = e;
		color = cl;
	}
	
	public static void draw(Graphics g ) {
		g.setColor(color);
		g.drawLine(col, start, col, end);
	}
}